import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  myBox = false;
  buttonText = "Écrire un post";
  newTitle = "";
  newContent = "";
  
  postsList = [
    {
      title: 'Mon dernier post',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur aspernatur eligendi tenetur non facilis explicabo illum eum quis? Omnis, porro labore? Quasi totam eveniet mollitia esse magnam reprehenderit quo vero?',
      loveIts: 40,
      created_at: 1568607660000
    },
    {
      title: 'Mon deuxième post',
      content: '2ème Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur aspernatur eligendi tenetur non facilis explicabo illum eum quis? Omnis, porro labore? Quasi totam eveniet mollitia esse magnam reprehenderit quo vero?',
      loveIts: -12,
      created_at: 1568607655000
    },
    {
      title: 'Mon premier post',
      content: 'Et encore un Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur aspernatur eligendi tenetur non facilis explicabo illum eum quis? Omnis, porro labore? Quasi totam eveniet mollitia esse magnam reprehenderit quo vero?',
      loveIts: 0,
      created_at: 1568607515000
    }
  ];

  onToggleBox() {
    this.myBox = !this.myBox;
    this.buttonText = this.myBox ? "Fermer": "Écrire un post";

  }

  onNewPost() {
    this.postsList.unshift({title: this.newTitle, content: this.newContent, loveIts: 0, created_at: Date.now()});
    this.newTitle = "";
    this.newContent = "";
    this.onToggleBox();
  }
  
}
